#!/bin/bash
#docker run --rm --link pgs_cohmetrix:pgs_cohmetrix --link palavras:palavras -v /home/sidleal/coh-metrix-nilc:/opt/text_metrics cohmetrix:focal bash -c "python3 run_all.py \"$1\""

docker stop pgs_cohmetrix
docker rm pgs_cohmetrix


docker volume create pgdatacometrix


docker run --name pgs_cohmetrix -e POSTGRES_USER=cohmetrix -e POSTGRES_PASSWORD=cohmetrix -v $PWD/tools/postgres:/shared -v pgdatacometrix:/var/lib/postgresql/data -d postgres



#docker run --rm --link pgs_cohmetrix:pgs_cohmetrix cohmetrix:focal bash -c "python3 run_all.py \"$1\""
docker run --rm -p 8080:8080 --link pgs_cohmetrix:pgs_cohmetrix nilcmetrix:1.0