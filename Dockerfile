# cohmetrix
FROM ubuntu:focal

ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt update && \ 
    apt install -y python3 python3-pip python3-numpy default-jre libpq-dev build-essential &&\
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


RUN pip3 install --upgrade pip && \
    pip3 install --upgrade setuptools 

WORKDIR /opt/text_metrics

COPY . .

RUN pip3 install https://github.com/kpu/kenlm/archive/master.zip
RUN pip3 install psycopg2-binary

RUN pip3 install ./idd3 
 #   - wget -O tools.zip https://drive.google.com/uc?id=1Ondvnz09RWDAX-1u3GIaXuAmkfKbGtqc&export=download
 #   - unzip tools.zip    


RUN pip3 install --no-cache-dir -r requirements.txt


RUN python3 -m nltk.downloader all



WORKDIR /opt/text_metrics
